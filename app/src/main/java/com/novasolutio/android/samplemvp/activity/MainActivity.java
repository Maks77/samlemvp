package com.novasolutio.android.samplemvp.activity;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.novasolutio.android.samplemvp.R;
import com.novasolutio.android.samplemvp.fragment.AddFragment;
import com.novasolutio.android.samplemvp.fragment.ListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
    }

    private void initUI() {
        //Спецом не нагружаю активность презентером и логикой. Все что делает активити - встраивает на экран два фрагмента одновременно.
        ListFragment listFragment = ListFragment.getInstance();
        AddFragment addFragment = AddFragment.getInstance();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_holder1, listFragment, ListFragment.TAG)
                .add(R.id.fragment_holder2, addFragment, AddFragment.TAG)
                .commit();
    }


}
