package com.novasolutio.android.samplemvp.repository;

import com.novasolutio.android.samplemvp.entity.Note;

import java.util.List;

public interface IRepository {

    List<Note> getAllNotes();
    void addNote(Note note);

}
