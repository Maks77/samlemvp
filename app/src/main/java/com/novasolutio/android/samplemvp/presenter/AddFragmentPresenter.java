package com.novasolutio.android.samplemvp.presenter;

import com.novasolutio.android.samplemvp.entity.Note;
import com.novasolutio.android.samplemvp.helpers.GeneratorId;
import com.novasolutio.android.samplemvp.repository.IRepository;
import com.novasolutio.android.samplemvp.repository.Repository;
import com.novasolutio.android.samplemvp.view.IAddFragmentView;

import java.util.Date;

public class AddFragmentPresenter {

    private IAddFragmentView mView;
    private IRepository mRepository;

    public AddFragmentPresenter() {
        mRepository = Repository.getInstance();
    }

    //Привязка вьюхи к презентеру
    public void bindView(IAddFragmentView view) {
        mView = view;
    }

    public void onAddBtnClicked(String inputValue) {
        if (inputValue.isEmpty()) {
             mView.showError("Input is empty...");
            return;
        }
        //Создаем новую запись исходя из текста, полученного с инпута
        Note newNote = new Note(GeneratorId.createID(), inputValue, new Date().getTime());

        //Говорим репозиторию добавить запись в массив (имитация добавления в базу данных)
        mRepository.addNote(newNote);

        //Очищаем инпут после записи
        mView.clearInput();
    }

}
