package com.novasolutio.android.samplemvp.view;

import com.novasolutio.android.samplemvp.entity.Note;

import java.util.ArrayList;

public interface IListFragmentView {
    void showList(ArrayList<Note> notes);
}
