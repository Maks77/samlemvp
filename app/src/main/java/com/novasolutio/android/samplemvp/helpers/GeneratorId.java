package com.novasolutio.android.samplemvp.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class GeneratorId {
    public static int createID(){
        Date now = new Date();
        return Integer.parseInt(new SimpleDateFormat("ddHHmmss",  Locale.US).format(now));
    }
}
