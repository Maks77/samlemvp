package com.novasolutio.android.samplemvp.presenter;

import com.novasolutio.android.samplemvp.entity.Note;
import com.novasolutio.android.samplemvp.repository.IRepository;
import com.novasolutio.android.samplemvp.repository.Repository;
import com.novasolutio.android.samplemvp.view.IListFragmentView;

import java.util.ArrayList;

public class ListFragmentPresenter {

    private IListFragmentView mView;
    private IRepository mRepository;

    public ListFragmentPresenter() {
        mRepository = Repository.getInstance();
    }

    public void bindView(IListFragmentView view) {
        mView = view;
    }

    public void onViewCreated() {
        ArrayList<Note> notes = (ArrayList<Note>) mRepository.getAllNotes();
        mView.showList(notes);
    }

}
