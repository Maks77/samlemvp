package com.novasolutio.android.samplemvp.entity;

public class Note {
    private int id;
    private String noteText;
    private long date;

    public Note(int id, String noteText, long date) {
        this.id = id;
        this.noteText = noteText;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }


    @Override
    public String toString() {
        return "Note: {id: " + id + "; "
                + "noteText: " + noteText + "; "
                + "date: " + date + "}";
    }
}
